import Layout from "../../components/layout/Layout";
import VerticalMenu from "../../components/ui/vertical-menu/VerticalMenu";
import PageHeader from "../../components/page-header/PageHeader";
import FAQ from "../../components/faq/FAQ";
import CTASimple from "../../components/cta/CTASimple";
import { useRouter } from 'next/router'

const navigation = [
    { name: 'Dashboard', href: '#', current: true, count: '5' },
    { name: 'Team', href: '#', current: false },
    { name: 'Projects', href: '#', current: false, count: '19' },
    { name: 'Calendar', href: '#', current: false, count: '20+' },
    { name: 'Documents', href: '#', current: false },
    { name: 'Reports', href: '#', current: false },
]
const faqs = [
    {
        question: "What's the best thing about Switzerland?1",
        answer:
            "I don't know, but the flag is a big plus. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas cupiditate laboriosam fugiat.",
    },

    {
        question: "What's the best thing about Switzerland?2",
        answer:
            "I don't know, but the flag is a big plus. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas cupiditate laboriosam fugiat.",
    },
    {
        question: "What's the best thing about Switzerland?3",
        answer:
            "I don't know, but the flag is a big plus. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas cupiditate laboriosam fugiat.",
    },
    {
        question: "What's the best thing about Switzerland?4",
        answer:
            "I don't know, but the flag is a big plus. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas cupiditate laboriosam fugiat.",
    },

]

export default function Faqs() {
    const router = useRouter()
    console.log(router.pathname)
    console.log(router.query)
    return(
        <Layout>
            <div className="bg-gray-50">
                <PageHeader title="Frequently asked questions" img="/assets/faq.jpeg"/>
                <div className="relative mx-auto max-w-7xl">
                    <div className="flex p-8">
                        <div className="w-1/4 px-4 sm:py-8 sm:px-6 lg:px-8">
                            <VerticalMenu navigation={navigation}/>
                        </div>
                        <div className="w-3/4">
                            <FAQ faqs={faqs}/>
                        </div>
                    </div>
                </div>
                <CTASimple/>
            </div>
        </Layout>
    )
}


