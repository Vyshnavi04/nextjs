import Layout from "../../components/layout/Layout";
import Toggle from "../../components/ui/toggle/Toggle";
import VerticalMenu from "../../components/ui/vertical-menu/VerticalMenu";
import PageHeader from "../../components/page-header/PageHeader";
import FAQ from "../../components/faq/FAQ";
import CTASimple from "../../components/cta/CTASimple";
import SuccessStory from "../../components/sucess/SuccessStroy";
import CallToActionOptionA from "../../components/cta/CallToActionOptionA";
import StoryCardOptionB from "../../components/courses/story/StoryCardOptionB";
import SuccessStoryCard from "../../components/sucess/SuccessStoryCard";
const navigation = [
    { name: 'Dashboard', href: '#', current: true, count: '5' },
    { name: 'Team', href: '#', current: false },
    { name: 'Projects', href: '#', current: false, count: '19' },
    { name: 'Calendar', href: '#', current: false, count: '20+' },
    { name: 'Documents', href: '#', current: false },
    { name: 'Reports', href: '#', current: false },
]
const faqs = [
    {
        question: "What's the best thing about Switzerland?",
        answer:
            "I don't know, but the flag is a big plus. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas cupiditate laboriosam fugiat.",
    },

    {
        question: "What's the best thing about Switzerland?",
        answer:
            "I don't know, but the flag is a big plus. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas cupiditate laboriosam fugiat.",
    },
    {
        question: "What's the best thing about Switzerland?",
        answer:
            "I don't know, but the flag is a big plus. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas cupiditate laboriosam fugiat.",
    },
    {
        question: "What's the best thing about Switzerland?",
        answer:
            "I don't know, but the flag is a big plus. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas cupiditate laboriosam fugiat.",
    },

]

export default function SuccessStoryPage() {
    return(
        <Layout>
            <div className="bg-gray-50">
                <PageHeader title="Few Success Stories" img="https://2uc5db1v032h5jwcm2gal07i-wpengine.netdna-ssl.com/wp-content/uploads/2019/12/How-to-Align-Your-Career-With-Your-Personal-Definition-of-Success-1024x682.jpg"/>
                <StoryCardOptionB />
                <div className="relative mx-auto max-w-7xl">
                    <div className="flex p-8 sm:flex-col md:flex-row">
                        <div className="px-4 md:w-1/4 sm:hidden md:block sm:py-8 sm:px-6 lg:px-8">
                            <h2 className="px-4 py-4 mb-4 text-gray-200 bg-blue-800 rounded-lg">Browse by Program</h2>
                            <VerticalMenu navigation={navigation}/>
                        </div>
                        <div className="w-3/4 py-16 sm:w-full">
                            { faqs.map(
                                faq=>(<SuccessStoryCard/>)
                            )}
                        </div>
                    </div>
                </div>
                <CTASimple/>
            </div>
        </Layout>
    )
}


