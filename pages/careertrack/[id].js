import { useRouter } from "next/router";
import Layout from "../../components/layout/Layout";
import CourseTab from "../../components/ui/tabs/CourseTab";
import CurriculumSection from "../../components/courses/curriculum/CurriculumSection";
import StoryCardOptionB from "../../components/courses/story/StoryCardOptionB";
import HiringCloud from "../../components/logocloud/HiringCloud";
import TestMonialCard from "../../components/testimonials/TestMonialCard";
import BlogList from "../../components/sections/blogs/BlogList";
import CallToActionOptionA from "../../components/cta/CallToActionOptionA";
import FeatureList from "../../components/feature/FeatureList";
import CareerTrackHero from "../../components/courses/hero/CarrerTrackHero";
import MentorList from "../../components/mentorlist/MentorList";


const CourseName = (props) => {
    const router = useRouter();
    const { id } = router.query;
    return (
         <Layout title='Learning Java'>
            <CareerTrackHero></CareerTrackHero>
             <StoryCardOptionB></StoryCardOptionB>
             <CurriculumSection>
                <CourseTab></CourseTab>
             </CurriculumSection>
             <FeatureList></FeatureList>
             <HiringCloud></HiringCloud>
             <CallToActionOptionA></CallToActionOptionA>
             <BlogList></BlogList>
             <MentorList></MentorList>
             <TestMonialCard></TestMonialCard>
         </Layout>
    )
}
export default CourseName;
