import Layout from "../../components/layout/Layout";
import PageHeader from "../../components/page-header/PageHeader";
import ContactForm from "../../components/sections/contact-form/ContactForm";

const title = 'Contact Us | Pragra - Best Classroom Trainings | Canada | US | India'
const contactDetails = [
    { name: 'Collaborate', email: 'support@example.com', telephone: '+1 (555) 123-4567' },
    { name: 'Press', email: 'support@example.com', telephone: '+1 (555) 123-4567' },
    { name: 'Join our team', email: 'support@example.com', telephone: '+1 (555) 123-4567' },
    { name: 'Say hello', email: 'support@example.com', telephone: '+1 (555) 123-4567' },
]
const locations = [
    { city: 'Los Angeles', address: ['4556 Brendan Ferry', 'Los Angeles, CA 90210'] },
    { city: 'New York', address: ['886 Walter Streets', 'New York, NY 12345'] },
    { city: 'Toronto', address: ['7363 Cynthia Pass', 'Toronto, ON N3Y 4H8'] },
    { city: 'Chicago', address: ['726 Mavis Island', 'Chicago, IL 60601'] },
]
export default function ContactusPage (){
    return(
        <Layout title={title}>
            <PageHeader title="Get in touch"/>

            <div className="bg-white">
                <div className="max-w-md mx-auto py-24 px-4 sm:max-w-3xl sm:py-32 sm:px-6 lg:max-w-7xl lg:px-8">
                    <div className="divide-y divide-warm-gray-200">
                        <section className="lg:grid lg:grid-cols-3 lg:gap-8" aria-labelledby="contactHeading">
                            <h2 id="contactHeading" className="text-2xl font-extrabold text-warm-gray-900 sm:text-3xl">
                                Get in touch
                            </h2>
                            <div className="mt-8 grid grid-cols-1 gap-12 sm:grid-cols-2 sm:gap-x-8 sm:gap-y-12 lg:mt-0 lg:col-span-2">
                                {contactDetails.map((item) => (
                                    <div key={item.name}>
                                        <h3 className="text-lg font-medium text-warm-gray-900">{item.name}</h3>
                                        <dl className="mt-2 text-base text-warm-gray-500">
                                            <div>
                                                <dt className="sr-only">Email</dt>
                                                <dd>{item.email}</dd>
                                            </div>
                                            <div className="mt-1">
                                                <dt className="sr-only">Phone number</dt>
                                                <dd>{item.telephone}</dd>
                                            </div>
                                        </dl>
                                    </div>
                                ))}
                            </div>
                        </section>
                        <section className="mt-16 pt-16 lg:grid lg:grid-cols-3 lg:gap-8" aria-labelledby="locationHeading">
                            <h2 id="locationHeading" className="text-2xl font-extrabold text-warm-gray-900 sm:text-3xl">
                                Locations
                            </h2>
                            <div className="mt-8 grid grid-cols-1 gap-12 sm:grid-cols-2 sm:gap-x-8 sm:gap-y-12 lg:mt-0 lg:col-span-2">
                                {locations.map((location) => (
                                    <div key={location.city}>
                                        <h3 className="text-lg font-medium text-warm-gray-900">{location.city}</h3>
                                        <div className="mt-2 text-base text-warm-gray-500 space-y-1">
                                            {location.address.map((line) => (
                                                <p key={line}>{line}</p>
                                            ))}
                                        </div>
                                    </div>
                                ))}
                            </div>
                        </section>
                    </div>
                </div>
            </div>
            <div className="bg-gray-50">
                <div className="">
                    <ContactForm/>
                </div>
            </div>

        </Layout>
    )
}
