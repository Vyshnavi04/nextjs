import Layout from "../../components/layout/Layout";
import Toggle from "../../components/ui/toggle/Toggle";
import VerticalMenu from "../../components/ui/vertical-menu/VerticalMenu";
import PageHeader from "../../components/page-header/PageHeader";
import OurStory from "../../components/sections/our-story/OurStory";
import Gallery from "react-photo-gallery";
import Team from "../../components/sections/team/Team";
import Partners from "../../components/sections/partners/Partners";
export const photos = [
    {
        src: "https://source.unsplash.com/2ShvY8Lf6l0/800x599",
        width: 4,
        height: 3
    },
    {
        src: "https://source.unsplash.com/Dm-qxdynoEc/800x799",
        width: 1,
        height: 1
    },
    {
        src: "https://source.unsplash.com/qDkso9nvCg0/600x799",
        width: 3,
        height: 4
    },
    {
        src: "https://source.unsplash.com/iecJiKe_RNg/600x799",
        width: 3,
        height: 4
    },
    {
        src: "https://source.unsplash.com/epcsn8Ed8kY/600x799",
        width: 3,
        height: 4
    },
    {
        src: "https://source.unsplash.com/NQSWvyVRIJk/800x599",
        width: 4,
        height: 3
    },
    {
        src: "https://source.unsplash.com/zh7GEuORbUw/600x799",
        width: 3,
        height: 4
    },
    {
        src: "https://source.unsplash.com/PpOHJezOalU/800x599",
        width: 4,
        height: 3
    },
    {
        src: "https://source.unsplash.com/I1ASdgphUH4/800x599",
        width: 4,
        height: 3
    },
    {
        src: "https://source.unsplash.com/XiDA78wAZVw/600x799",
        width: 3,
        height: 4
    },
    {
        src: "https://source.unsplash.com/x8xJpClTvR0/800x599",
        width: 4,
        height: 3
    },
    {
        src: "https://source.unsplash.com/u9cG4cuJ6bU/4927x1000",
        width: 4927,
        height: 1000
    },
    {
        src: "https://source.unsplash.com/qGQNmBE7mYw/800x599",
        width: 4,
        height: 3
    }
];




export default function About() {
    return(
        <Layout>
            <PageHeader title="About Us"/>
                <div className="py-8 w-full xl:py-16 px-4 sm:px-6 lg:px-8 bg-white overflow-hidden">
                    <div className="max-w-max lg:max-w-7xl mx-auto">
                        <div className="relative">
                            <svg
                                className="hidden md:block absolute top-0 right-0 mt-20 -mr-20"
                                width={404}
                                height={384}
                                fill="none"
                                viewBox="0 0 404 384"
                                aria-hidden="true"
                            >
                                <defs>
                                    <pattern
                                        id="95e8f2de-6d30-4b7e-8159-f791729db21b"
                                        x={0}
                                        y={0}
                                        width={20}
                                        height={20}
                                        patternUnits="userSpaceOnUse"
                                    >
                                        <rect x={0} y={0} width={4} height={4} className="text-gray-200" fill="currentColor" />
                                    </pattern>
                                </defs>
                                <rect width={404} height={384} fill="url(#95e8f2de-6d30-4b7e-8159-f791729db21b)" />
                            </svg>
                            <svg
                                className="hidden md:block absolute bottom-0 left-0 mb-20 -ml-20"
                                width={404}
                                height={384}
                                fill="none"
                                viewBox="0 0 404 384"
                                aria-hidden="true"
                            >
                                <defs>
                                    <pattern
                                        id="7a00fe67-0343-4a3c-8e81-c145097a3ce0"
                                        x={0}
                                        y={0}
                                        width={20}
                                        height={20}
                                        patternUnits="userSpaceOnUse"
                                    >
                                        <rect x={0} y={0} width={4} height={4} className="text-gray-200" fill="currentColor" />
                                    </pattern>
                                </defs>
                                <rect width={404} height={384} fill="url(#7a00fe67-0343-4a3c-8e81-c145097a3ce0)" />
                            </svg>
                            <div className="relative md:bg-white md:p-6">
                                <div className="lg:grid lg:grid-cols-1 lg:gap-6">
                                    <div className="prose prose-blue prose-lg text-gray-500 lg:max-w-none">
                                        <p>
                                            Pragra is a leading technology company leveraging the power of AI and digital era to automate your product development and test automation. We assist our customer to optimize their process to maximize their profits/savings.

                                            We strive to be thought leaders, not industry followers and always stay abreast of the latest products, methods, and technology so you don’t have to. We nourished in our People and their capability and our framework to support the achievement of high performance, both from an individual and organizational perspective.

                                            We have a proven track record of delivery and our focus is to partner with our clients to understand their testing needs and help them put in place the right resourcing, bootcamp or service solutions to meet their every unique requirement.
                                        </p>

                                    </div>
                            </div>
                            </div>
                        </div>
                    </div>

            </div>

            {/* our story section start*/}
            <OurStory/>
            {/* our story section end*/}
            <div className="max-w-max lg:max-w-7xl bg-white py-10 mx-auto">
                <h2 className="text-4xl text-gray-600 text-center font-extrabold py-4 uppercase">Pragra in Action</h2>
                <Gallery photos={photos} />
            </div>
            <Team/>

            <Partners/>
        </Layout>
    )
}


