/* This example requires Tailwind CSS v2.0+ */
import { Fragment } from 'react'
import { Popover, Transition } from '@headlessui/react'
import {
  AnnotationIcon,
  ChatAlt2Icon,
  InboxIcon,
  MenuIcon,
  QuestionMarkCircleIcon,
  XIcon,
} from '@heroicons/react/outline'
import LogoCloud from '../components/logocloud/logocloud'
import HeroB from '../components/hero/HeroB'
import Slider from '../components/sliders/Slider'
import HiringCloud from '../components/logocloud/HiringCloud'
import Stats from '../components/stats/Stats'
import CardList from "../components/cardlist/CardList";
import AssuredP from "../components/sections/assured/AssuredP";
import Advantage from "../components/sections/advantage/Advantage";
import Mentors from "../components/sections/mentors/Mentors";
import BlogList from "../components/sections/blogs/BlogList";
import BecomeInstructor from "../components/sections/instructor/BecomeInstructor";
import Layout from "../components/layout/Layout";
import fetch  from 'isomorphic-fetch';
import SuccessStory from "../components/sucess/SuccessStroy";

const title = 'Pragra | Best Instructor Lead Training| Java | DevOps | React | Angular'

function classNames(...classes) {
  return classes.filter(Boolean).join(' ')
}

export function Home( { testimonials }) {
  return (

    <div className="min-h-screen bg-white">
      <Layout title={title}>
      {/*<HeroOptionA> </HeroOptionA> */}
      <HeroB></HeroB>
      <LogoCloud></LogoCloud>
      <Slider>
          {testimonials.map( testimonial=> <SuccessStory key={testimonial.id} story={testimonial}/> )}
      </Slider>
      <HiringCloud></HiringCloud>
      <Stats></Stats>
      <CardList></CardList>
      <AssuredP></AssuredP>
        <Advantage></Advantage>
        <Mentors></Mentors>
        <BecomeInstructor></BecomeInstructor>
        <BlogList></BlogList>
      </Layout>
    </div>
  )
}

Home.getInitialProps = async (ctx) => {
    const res = await fetch('https://pragra.io/api/testimonial')
    const json = await res.json()
    return {testimonials: json }
}
export default Home;
