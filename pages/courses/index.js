import CourseTab from "../../components/ui/tabs/CourseTab";
import PageHeader from "../../components/page-header/PageHeader";
import Layout from "../../components/layout/Layout";
import CourseGrid from "../../components/courses/coursegrid/CourseGrid";
import FeatureList from "../../components/feature/FeatureList";
import CallToAction from "../../components/calltoaction";
import CallToActionOptionA from "../../components/cta/CallToActionOptionA";
import Mentors from "../../components/sections/mentors/Mentors";
import BecomeInstructor from "../../components/sections/instructor/BecomeInstructor";
import TestMonialCard from "../../components/testimonials/TestMonialCard";
import HiringCloud from "../../components/logocloud/HiringCloud";
import BreadCrumb from "../../components/ui/breadcrumb/BreadCrumb";


export default function CoursePage (){
    return(
        <Layout>
            <PageHeader title="Courses"/>
            <BreadCrumb/>
            <CourseGrid>
            </CourseGrid>
            <HiringCloud/>
            <FeatureList/>
            <CallToActionOptionA/>
            <BecomeInstructor/>
            <TestMonialCard/>
        </Layout>
    )
}
