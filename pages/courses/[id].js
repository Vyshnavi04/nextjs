import { useRouter } from "next/router";
import HeroOptionA from "../../components/hero/hero.component";
import Layout from "../../components/layout/Layout";
import CourseTab from "../../components/ui/tabs/CourseTab";
import CurriculumSection from "../../components/courses/curriculum/CurriculumSection";
import CourseHero from "../../components/courses/hero/CourseHero";
import StoryCard from "../../components/courses/story/StoryCard";
import StoryCardOptionB from "../../components/courses/story/StoryCardOptionB";
import HiringCloud from "../../components/logocloud/HiringCloud";
import TestMonialCard from "../../components/testimonials/TestMonialCard";
import BlogList from "../../components/sections/blogs/BlogList";
import CallToActionOptionA from "../../components/cta/CallToActionOptionA";
import FeatureList from "../../components/feature/FeatureList";
import CourseStat from "../../components/stats/CourseStat";
import MentorList from "../../components/mentorlist/MentorList";


const CourseName = (props) => {
    const router = useRouter();
    const { id } = router.query;
    return (
         <Layout title='Learning Java'>
            <CourseHero></CourseHero>
             <CourseStat></CourseStat>
             <CurriculumSection>
                <CourseTab></CourseTab>
             </CurriculumSection>
             <FeatureList></FeatureList>
             <HiringCloud></HiringCloud>
             <BlogList></BlogList>
             <MentorList></MentorList>
             <TestMonialCard></TestMonialCard>
         </Layout>
    )
}
export default CourseName;
