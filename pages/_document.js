import Document, {Head, Main, NextScript, Html} from "next/document";

export default class PragraDocument extends Document {
    render() {
        return (
            <Html>
                <Head>
                    <meta name="description" content="Instructor Lead Training GTA" />
                    <link rel='shortcut icon' type='image/svg' href='/pround.svg'/>
                    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/nprogress/0.2.0/nprogress.min.css'/>
                </Head>
                <Main/>
                <NextScript>

                </NextScript>
            </Html>
        )
    }
}
