import Layout from "../../components/layout/Layout";
import PageHeader from "../../components/page-header/PageHeader";
import ContactForm from "../../components/sections/contact-form/ContactFormMentor";
import MentorList from '../../components/mentorlist/MentorList'
import FAQ from '../../components/sections/faqs/faq'
import Advantage from '../../components/sections/advantage/Advantage'
const title = 'Become A Mentor | Pragra - Best Classroom Trainings | Canada | US | India'

export default function BecomeAMentorPage (){
    return(
        <Layout title={title}>
            <PageHeader title="Become A mentor"/>

            <div className="bg-white">
                <div className="max-w-md mx-auto py-24 px-4 sm:max-w-3xl sm:py-32 sm:px-6 lg:max-w-7xl lg:px-8">
                    <div className="">
                        <section className="text-center" aria-labelledby="contactHeading">
                            <h2 id="Heading" className="text-2xl font-extrabold text-warm-gray-900 sm:text-3xl">
                               Join As A Mentor
                            </h2>
                            <p className='mt-5'> 
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eget venenatis ante.
                             Praesent in quam volutpat sem rutrum congue at nec augue. Vestibulum tincidunt, 
                             tellus in molestie lacinia, ligula justo vestibulum dui, vel feugiat nibh orci sit amet dolor. 
                             Integer ultricies nulla ac nisi imperdiet vestibulum. Nunc ac magna eget tortor lacinia finibus. 
                             Maecenas in metus et arcu ultricies dictum vel at velit. Phasellus tortor eros, dictum vel augue ac, 
                             faucibus mattis sem. Suspendisse euismod, mi non pellentesque suscipit, lacus nisl sollicitudin velit, 
                             ac congue mauris dui ac nunc. Ut quis ligula sed lacus ultricies dictum et viverra dolor.
                              Nullam blandit interdum mauris. Duis massa sem, fringilla ac libero et, pellentesque rutrum risus. Quisque lorem ligula, consectetur at cursus quis, efficitur lobortis nibh. Vestibulum fringilla ex sit amet purus porttitor aliquet vel vitae neque. Phasellus efficitur, magna nec dignissim molestie, neque sem sagittis nunc, nec pulvinar est lorem vestibulum libero. Suspendisse bibendum scelerisque nulla, 
                            non suscipit leo laoreet eget. Aenean molestie felis a risus mollis mattis.
                            </p>
                            <div className="items-center m-10">
                                <button
                                   
                                    className="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-blue-600 hover:bg-grape-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-grape-500"
                                >
                                    Become a mentor
                                </button>
                            </div>
                        </section>
                        
                        <div className="flex justify-center items-center p-4 ">
                            <div className=" flex-1 p-4 rounded-lg shadow-xl max-w-full sm:p-8 ">
                            <div className="aspect-w-16 aspect-h-5">
                                <iframe  src="https://www.youtube.com/embed/R_OERlafbmw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                        </div>
                        </div>
                        <div className="mt-20">
                            <Advantage/>    
                        </div>
                        <section className="mt-20">
                            <MentorList/>
                           
                            
                        </section>
                        <div className="mt-20">
                            <FAQ/>  
                        </div>
                    </div>
                </div>
            </div>
            
            <div className="bg-gray-50">
                <div className="">
                    <ContactForm />
                </div>
            </div>

        </Layout>
    )
}
