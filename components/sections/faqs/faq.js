
export default function faq() {
    return (
        <div className="bg-white mt-5">
            <section className= "h-20">
                            <div className="flex  p-5 border border-gray-500  rounded">
                                <div className="w-3/4  h-20">
                                    <h2 className="text-2xl font-extrabold text-warm-gray-900 sm:text-3xl">Frequently Asked Questions </h2>
                                    <p className="py-1"> Some text goes here</p>
                                </div>
                                <div className="w-1/4  h-12"> 
                                <button className="flex-end  ml-40 bg-transparent hover:bg-gray-500 text-gray-700 font-semibold hover:text-white py-2 px-4 border border-gray-500 hover:border-transparent rounded">
                                    View All
                                </button>
                                </div>
                            </div>
            </section>
        </div>
    )
}
