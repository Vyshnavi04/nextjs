/*
  This example requires Tailwind CSS v2.0+

  This example requires some changes to your config:

  ```
  // tailwind.config.js
  module.exports = {
    // ...
    plugins: [
      // ...
      require('@tailwindcss/forms'),
    ]
  }
  ```
*/
import { useState } from 'react'
import { Switch } from '@headlessui/react'
import Modal from '../ui/modal/Modal'

function classNames(...classes) {
    return classes.filter(Boolean).join(' ')
}

export default function PageContactForm({ bgColor }) {
    const [agreed, setAgreed] = useState(false)

    return (
        <div className= {classNames(bgColor, 'bg-opacity-60 py-12 rounded-lg overflow-hidden sm:px-6 lg:px-8 lg:py-8')}>
            <div className="relative max-w-xl mx-auto">
                <div className="mt-12">
                    <form action="#" method="POST" className="grid grid-cols-1 gap-y-4 sm:grid-cols-2 sm:gap-x-8">
                        <div>
                            {/*<label htmlFor="first_name" className="block text-sm font-medium text-gray-700">*/}
                            {/*    First name*/}
                            {/*</label>*/}
                            <div className="mt-1">
                                <input
                                    type="text"
                                    name="first_name"
                                    id="first_name"
                                    placeholder="First Name*"
                                    autoComplete="given-name"
                                    className="block w-full px-4 py-3 border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500"
                                />
                            </div>
                        </div>
                        <div>
                            {/*<label htmlFor="last_name" className="block text-sm font-medium text-gray-700">*/}
                            {/*    Last name*/}
                            {/*</label>*/}
                            <div className="mt-1">
                                <input
                                    type="text"
                                    name="last_name"
                                    id="last_name"
                                    placeholder="Last name*"
                                    autoComplete="family-name"
                                    className="block w-full px-4 py-3 border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500"
                                />
                            </div>
                        </div>
                        <div className="sm:col-span-2">
                            {/*<label htmlFor="email" className="block text-sm font-medium text-gray-700">*/}
                            {/*    Email*/}
                            {/*</label>*/}
                            <div className="mt-1">
                                <input
                                    id="email"
                                    name="email"
                                    type="email"
                                    autoComplete="email"
                                    placeholder="Email*"
                                    className="block w-full px-4 py-3 border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500"
                                />
                            </div>
                        </div>
                        <div className="sm:col-span-2">
                            {/*<label htmlFor="phone_number" className="block text-sm font-medium text-gray-700">*/}
                            {/*    Phone Number*/}
                            {/*</label>*/}
                            <div className="relative mt-1 rounded-md shadow-sm">
                                <div className="absolute inset-y-0 left-0 flex items-center">
                                    <label htmlFor="country" className="sr-only">
                                        Country
                                    </label>
                                    <select
                                        id="country"
                                        name="country"
                                        className="h-full py-0 pl-4 pr-8 text-gray-500 bg-transparent border-transparent rounded-md focus:ring-indigo-500 focus:border-indigo-500"
                                    >
                                        <option>US</option>
                                        <option>CA</option>
                                        <option>IN</option>
                                    </select>
                                </div>
                                <input
                                    type="text"
                                    name="phone_number"
                                    id="phone_number"
                                    autoComplete="tel"
                                    className="block w-full px-4 py-3 pl-20 border-gray-300 rounded-md focus:ring-indigo-500 focus:border-indigo-500"
                                    placeholder="Phone*"
                                />
                            </div>
                        </div>
                        <div className="sm:col-span-2">
                            {/*<label htmlFor="message" className="block text-sm font-medium text-gray-700">*/}
                            {/*    Message*/}
                            {/*</label>*/}
                            <div className="mt-1">
                <textarea
                    id="message"
                    name="message"
                    rows={3}
                    placeholder="Message"
                    className="block w-full px-4 py-3 border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500"
                    defaultValue={''}
                />
                            </div>
                        </div>
                        <div className="sm:col-span-2">
                            <div className="flex items-start">
                                <div className="flex-shrink-0">
                                    <Switch
                                        checked={agreed}
                                        onChange={setAgreed}
                                        className={classNames(
                                            agreed ? 'bg-indigo-600' : 'bg-gray-200',
                                            'relative inline-flex flex-shrink-0 h-6 w-11 border-2 border-transparent rounded-full cursor-pointer transition-colors ease-in-out duration-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500'
                                        )}
                                    >
                                        <span className="sr-only">Agree to policies</span>
                                        <span
                                            aria-hidden="true"
                                            className={classNames(
                                                agreed ? 'translate-x-5' : 'translate-x-0',
                                                'inline-block h-5 w-5 rounded-full bg-white shadow transform ring-0 transition ease-in-out duration-200'
                                            )}
                                        />
                                    </Switch>
                                </div>
                                <div className="ml-3">
                                    <p className="text-base text-gray-50">
                                        By selecting this, you agree to the{' '}
                                        <a href="#" className="font-medium text-gray-400 underline">
                                            Privacy Policy
                                        </a>{' '}
                                        and{' '}
                                        <a href="#" className="font-medium text-gray-400 underline">
                                            Cookie Policy
                                        </a>
                                        <Modal show={true}>
                                            <h1>Hello from Modal</h1>
                                        </Modal>
                                        .
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div className="sm:col-span-2">
                            <button
                                type="submit"
                                className="inline-flex items-center justify-center w-full px-6 py-3 text-base font-medium text-white bg-blue-600 border border-transparent rounded-md shadow-sm hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                            >
                                Apply Now
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}
