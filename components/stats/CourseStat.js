/* This example requires Tailwind CSS v2.0+ */
export default function CourseStat() {
    return (
        <div className="bg-gray-200">
            <div className="max-w-7xl mx-auto py-12 px-4 sm:py-16 sm:px-6 lg:px-8 lg:py-20">
                <div className="max-w-4xl mx-auto text-center">
                    <h2 className="text-3xl font-bold tracking-wide uppercase text-gray-900 sm:text-2xl">Course HighLights</h2>
                </div>
                <dl className="mt-10 text-center sm:max-w-7xl sm:mx-auto sm:grid sm:grid-cols-3 sm:gap-8">
                    <div className="flex flex-col">
                        <dt className="order-1 mb-4 text-lg leading-6 font-medium text-gray-700">ABC</dt>
                        <dt className="order-4 mt-4 text-lg leading-6 font-medium text-gray-700">Course Duration</dt>
                        <dd className="order-2  text-5xl  text-gray-900">100%</dd>
                    </div>
                    <div className="flex flex-col">
                        <dt className="order-1 mb-4 text-lg leading-6 font-medium text-gray-700">ABC</dt>
                        <dt className="order-4 mt-4 text-lg leading-6 font-medium text-gray-700">Course Duration</dt>
                        <dd className="order-2  text-5xl  text-gray-900">100%</dd>
                    </div>
                    <div className="flex flex-col">
                        <dt className="order-1 mb-4 text-lg leading-6 font-medium text-gray-700">ABC</dt>
                        <dt className="order-4 mt-4 text-lg leading-6 font-medium text-gray-700">Course Duration</dt>
                        <dd className="order-2  text-5xl  text-gray-900">100%</dd>
                    </div>

                </dl>
            </div>
        </div>
    )
}
