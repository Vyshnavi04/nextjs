import Card from "../card/Card";
import FeaturedCard from "../card/Card";
const cardData = {
    title: 'Java Full Stack',
    description: 'Learn Best Java Development Program'
}
const CardList = props=> {
  return (
      <div className="bg-gradient-to-r from-blue-200 to-blue-500 pb-6">
          <div className="pt-6 sm:pt-8 lg:pt-8">
              <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
                  <div className="text-center">
                      <h2 className="text-3xl font-bold text-gray-100 sm:text-2xl lg:text-3xl">Featured Career Tracks</h2>
                  </div>
              </div>
          </div>
          <FeaturedCard prog = {cardData}/>
          <FeaturedCard/>
          <FeaturedCard/>
      </div>
  )
};

export default CardList;
