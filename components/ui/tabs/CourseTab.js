import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import { resetIdCounter } from 'react-tabs';
import TabContent from "./TabContent";
const tabs = [
    {
        id: 1,
        tabname: 'Overview',
        tabcontent: {

        }
    },
    {
        id: 2,
        tabname: 'Curriculum',
        tabcontent: {

        }
    },
    {
        id: 3,
        tabname: 'Mentor',
        tabcontent: {

        }
    },
    {
        id: 4,
        tabname: 'Fee & Finance Options',
        tabcontent: {

        }
    },
    {
        id: 5,
        tabname: 'FAQs',
        tabcontent: {

        }
    },
    {
        id: 6,
        tabname: 'Schedule',
        tabcontent: {

        }
    }

]
const CourseTab = (props) => {
    resetIdCounter();
 return (
    <Tabs>
        <TabList>
            { tabs.map( tab=> <Tab key={tab.id}>{tab.tabname}</Tab>  ) }
        </TabList>

        <TabPanel>
            <h2><TabContent/></h2>
        </TabPanel>
        <TabPanel>
            <h2>Any content 2</h2>
        </TabPanel>
        <TabPanel>
            <h2>Any content 3</h2>
        </TabPanel>
        <TabPanel>
            <h2>Any content 3</h2>
        </TabPanel>
        <TabPanel>
            <h2>Any content 3</h2>
        </TabPanel>
        <TabPanel>
            <h2>Any content 3</h2>
        </TabPanel>
    </Tabs>
)
}

export default CourseTab;