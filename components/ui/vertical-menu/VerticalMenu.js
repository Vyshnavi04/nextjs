import Link from 'next/link'
function classNames(...classes) {
    return classes.filter(Boolean).join(' ')
}

export default function VerticalMenu( { navigation }) {
    return (
        <nav className="space-y-6" aria-label="Sidebar">
            {navigation.map((item) => (
                <Link   key={item.name} as = {`faqs/${item.name}`}
                        href= { {
                            pathname: '/faqs',
                            query: { name: `${item.name}` }
                        }}>
                <a
                    href={item.href}
                    className={classNames(
                        item.current ? 'bg-gray-100 text-gray-900' : 'text-gray-600 hover:bg-gray-50 hover:text-gray-900',
                        'group flex items-center px-3 py-2 text-sm font-medium rounded-md'
                    )}
                    aria-current={item.current ? 'page' : undefined}
                >
                    <span className="truncate">{item.name}</span>
                    {item.count ? (
                        <span
                            className={classNames(
                                item.current ? 'bg-white' : 'bg-blue-100 text-gray-600 group-hover:bg-gray-200',
                                'ml-auto inline-block py-0.5 px-3 text-xs rounded-full'
                            )}
                        >
              {item.count}
            </span>
                    ) : null}
                </a>
                </Link>
            ))}
        </nav>
    )
}
