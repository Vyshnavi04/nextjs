/* This example requires Tailwind CSS v2.0+ */
import { useState, } from 'react'
import React from "react";
import { Switch } from '@headlessui/react'
import {data} from "autoprefixer";

function classNames(...classes) {
    return classes.filter(Boolean).join(' ')
}

export default class Toggle extends React.Component {
    constructor(props) {
        super();
        const { name } = props;
        this.state = {
            data: {
                name: name,
                enabled: false
            }
        }
    }
    async onChange(name){
        await this.setState(
            { data : {
                    name: name,
                    enabled: !this.state.data.enabled
                }}
        )
        this.props.onSet(this.state.data)

    }
    render() {
        const { enabled, name } = this.state.data;
        return (
            <Switch.Group as="div" className="flex items-center justify-between py-1">
                <Switch.Label as="span" className="flex-grow flex flex-col" passive>
                    <span className="text-sm capitalize font-medium text-gray-700">{name}</span>
                 </Switch.Label>
                <Switch
                        checked={enabled}
                        onChange={()=>this.onChange(this.props.name)}
                        className={classNames(
                            enabled ? 'bg-blue-600' : 'bg-gray-200',
                            'relative inline-flex flex-shrink-0 h-6 w-11 border-2 border-transparent rounded-full cursor-pointer transition-colors ease-in-out duration-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500'
                        )}
                >
                    <span className="sr-only">Use setting</span>
                    <span
                        aria-hidden="true"
                        className={classNames(
                            enabled ? 'translate-x-5' : 'translate-x-0',
                            'pointer-events-none inline-block h-5 w-5 rounded-full bg-white shadow transform ring-0 transition ease-in-out duration-200'
                        )}
                    />
                </Switch>
            </Switch.Group>
        )
    }
}
