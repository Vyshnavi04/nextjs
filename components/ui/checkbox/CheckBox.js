import React from "react";

function classNames(...classes) {
    return classes.filter(Boolean).join(' ')
}

export default class CheckBox extends React.Component {
    constructor(props) {
        super();
        const { name } = props;
        this.state = {
            data: {
                name: name,
                enabled: false
            }
        }
    }
    async onChange(name){
        await this.setState(
            { data : {
                    name: name,
                    enabled: !this.state.data.enabled
                }}
        )
        this.props.onSet(this.state.data)

    }
    render() {
        const { enabled, name } = this.state.data;
        return (
            <div className="flex flex-col">
                <label className="inline-flex items-center justify-between py-2 leading-6 border-b-2 ">
                    <span className="mr-2">{name}</span>
                    <input type="checkbox"  onChange={()=>this.onChange(this.props.name)} className="w-6 h-6 p-2 text-indigo-600 border-gray-600 rounded-full bg-blue-50 form-checkbox" checked={enabled}/>
                </label>
            </div>
        )
    }
}
