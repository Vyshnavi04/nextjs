/* This example requires Tailwind CSS v2.0+ */
import { CheckIcon } from '@heroicons/react/outline'

export default function CurriculumSection( {children} ) {
    return (
        <div className="bg-white">
            <div className="max-w-7xl mx-auto py-16 px-4 sm:px-6 lg:py-24 lg:px-8">
                {/*<div className="max-w-3xl mb-3 mx-auto text-center">*/}
                {/*    <h2 className="text-3xl font-extrabold text-gray-900">All-in-one platform</h2>*/}
                {/*    <p className="mt-4 text-lg text-gray-500">*/}
                {/*        Ac euismod vel sit maecenas id pellentesque eu sed consectetur. Malesuada adipiscing sagittis vel nulla nec.*/}
                {/*    </p>*/}
                {/*</div>*/}
                {/*Divider*/}
                {children}
            </div>
        </div>
    )
}
