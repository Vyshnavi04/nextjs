/*
  This example requires Tailwind CSS v2.0+

  This example requires some changes to your config:

  ```
  // tailwind.config.js
  module.exports = {
    // ...
    plugins: [
      // ...
      require('@tailwindcss/forms'),
    ]
  }
  ```
*/

import React, {Fragment} from "react"
import Link from "next/link"
import { useRouter } from 'next/router'
import PageContactForm from "../../contactcard/PageContactForm";


function classNames(...classes) {
    return classes.filter(Boolean).join(' ')
}
const CareerTrackHero = (  ) => {
    const router = useRouter();
    const { query } = router;
    return (
        <section>
            <div className="relative bg-gray-800 sm:pb-24 md:py-4">
                <div className="absolute inset-x-0 bottom-0 h-80 xl:top-0 xl:h-full">
                    <div className="w-full h-full xl:grid xl:grid-cols-2">
                        <img className="object-cover w-full h-full opacity-20 xl:absolute xl:inset-0" src="https://images.unsplash.com/photo-1571844307880-751c6d86f3f3?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=2017&q=80"
                             alt="People working on laptops"/>
                    </div>
                </div>
            <div className="py-10 sm:pt-16 lg:pt-6 lg:pb-14 lg:overflow-hidden">
                <div className="mx-auto max-w-7xl lg:px-8">
                    <div className="lg:grid lg:grid-cols-2 lg:gap-8">
                        <div className="max-w-md px-4 mx-auto sm:max-w-2xl sm:px-6 sm:text-center lg:px-0 lg:text-left lg:flex lg:items-center">
                            <div className="lg:py-24">
                                <span className="px-4 py-2 text-sm font-semibold leading-5 tracking-wide text-white uppercase bg-green-800 rounded-full">
                          Career Track
                        </span>
                                <h1 className="mt-4 text-4xl font-extrabold tracking-tight text-white sm:mt-5 sm:text-6xl lg:mt-6 xl:text-6xl">
                                    <span className="block">Java Full Stack Developer</span>

                                </h1>
                                <p className="mt-3 text-base text-gray-300 sm:mt-5 sm:text-xl lg:text-lg xl:text-xl">
                                    Anim aute id magna aliqua ad ad non deserunt sunt. Qui irure qui Lorem cupidatat commodo. Elit
                                    sunt amet fugiat veniam occaecat fugiat.
                                </p>
                                <div className="mt-10 sm:mt-12">

                                </div>
                            </div>
                        </div>
                        <div className="-mb-16 sm:-mb-48 lg:m-0 lg:relative">
                            <div className="max-w-lg px-4 mx-auto sm:max-w-3xl sm:px-6 lg:max-w-none lg:px-0">
                             <PageContactForm bgColor="bg-gray-500"></PageContactForm>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            {/* More main page content here... */}
        </section>
    )
}
export default CareerTrackHero;
