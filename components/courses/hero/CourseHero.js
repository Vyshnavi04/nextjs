/*
  This example requires Tailwind CSS v2.0+

  This example requires some changes to your config:

  ```
  // tailwind.config.js
  module.exports = {
    // ...
    plugins: [
      // ...
      require('@tailwindcss/forms'),
    ]
  }
  ```
*/

import React, {Fragment} from "react"
import Link from "next/link"
import { useRouter } from 'next/router'
import { useState } from 'react'
import { CreditCardIcon, OfficeBuildingIcon, UserIcon, UsersIcon } from '@heroicons/react/solid';
import {Transition} from "@headlessui/react";
import TabContent from "../../ui/tabs/TabContent";
import PageContactForm from "../../contactcard/PageContactForm";


function classNames(...classes) {
    return classes.filter(Boolean).join(' ')
}
const CourseHero = (  ) => {
    const router = useRouter();
    const { query } = router;
    return (
        <section>
            <div className="relative bg-blue-900">
                <div className="absolute inset-x-0 bottom-0 h-80 xl:top-0 xl:h-full">
                    <div className="w-full h-full xl:grid xl:grid-cols-2">
                        <img className="object-cover w-full h-full opacity-20 xl:absolute xl:inset-0" src="https://images.unsplash.com/photo-1561736778-92e52a7769ef?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80"
                             alt="People working on laptops"/>
                    </div>
                </div>
            <div className="pt-10 sm:pt-16 lg:pt-6 lg:pb-14 lg:overflow-hidden">
                <div className="mx-auto max-w-7xl lg:px-8">
                    <div className="lg:grid lg:grid-cols-2 lg:gap-8">
                        <div className="-mb-16 sm:-mb-48 lg:m-0 lg:relative">
<<<<<<< Updated upstream
                            <div className="max-w-lg px-4 mx-auto sm:max-w-3xl sm:px-6 lg:max-w-none lg:px-0">
=======
                            <div className="mx-auto max-w-lg px-4 sm:max-w-3xl sm:px-6 lg:max-w-none lg:px-0">
>>>>>>> Stashed changes
                                <PageContactForm bgColor="bg-green-500"/>
                            </div>
                        </div>
                        <div className="max-w-md px-4 mx-auto sm:max-w-2xl sm:px-6 sm:text-center lg:px-0 lg:text-left lg:flex lg:items-center">
                            <div className="lg:py-24">
                                <span className="px-4 py-2 text-sm font-semibold leading-5 tracking-wide text-white uppercase bg-red-800 rounded-full">
                          Course
                        </span>
                                <h1 className="mt-4 text-4xl font-extrabold tracking-tight text-white sm:mt-5 sm:text-6xl lg:mt-6 xl:text-6xl">
                                    <span className="block">AWS Certification</span>

                                </h1>
                                <p className="mt-3 text-base text-gray-300 sm:mt-5 sm:text-xl lg:text-lg xl:text-xl">
                                    Anim aute id magna aliqua ad ad non deserunt sunt. Qui irure qui Lorem cupidatat commodo. Elit
                                    sunt amet fugiat veniam occaecat fugiat.
                                </p>
                                <div className="mt-10 sm:mt-12">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            {/* More main page content here... */}
        </section>
    )
}
export default CourseHero;
