import { ChevronRightIcon, StarIcon } from '@heroicons/react/solid'

const stats = [
    { label: 'Course Duration', value: '5 Months' },
    { label: 'Enroll By', value: '19 May 2021' },
    { label: 'Location', value: 'Mississauga, CA' },
    { label: 'Type', value: 'ClassRoom' },
]

const getStats = (enableStats)=>{
    if(enableStats) {
        return  (
            stats.map((stat) => (
                <div key={stat.label} className="pt-6 border-t-2 border-gray-100">
                    <dt className="text-base font-medium text-gray-500">{stat.label}</dt>
                    <dd className="text-2xl font-bold tracking-tight text-gray-900">{stat.value}</dd>
                </div>
            )))
    }
}

export default function StoryCardOptionB( {enableStats}) {
    return (
        <div className="bg-gradient-to-r from-gray-50 via-blue-50 to-gray-100 sm:mt-12 lg:mt-0">
            <main>

                {/* Testimonial/stats section */}
                <div className="relative sm:py-24 md:py-24 lg:py-8">
                    <div className="lg:mx-auto lg:max-w-7xl lg:px-8 lg:grid lg:grid-cols-2 lg:gap-24 lg:items-start">
                        <div className="relative sm:py-16 lg:py-0">
                            <div aria-hidden="true" className="hidden sm:block lg:absolute lg:inset-y-0 lg:right-0 lg:w-screen">
                                <div className="absolute inset-y-0 w-full right-1/2 bg-gray-50 rounded-r-3xl lg:right-72" />
                                <svg
                                    className="absolute -ml-3 top-8 left-1/2 lg:-right-8 lg:left-auto lg:top-12"
                                    width={404}
                                    height={392}
                                    fill="none"
                                    viewBox="0 0 404 392"
                                >
                                    <defs>
                                        <pattern
                                            id="02f20b47-fd69-4224-a62a-4c9de5c763f7"
                                            x={0}
                                            y={0}
                                            width={20}
                                            height={20}
                                            patternUnits="userSpaceOnUse"
                                        >
                                            <rect x={0} y={0} width={4} height={4} className="text-gray-200" fill="currentColor" />
                                        </pattern>
                                    </defs>
                                    <rect width={404} height={392} fill="url(#02f20b47-fd69-4224-a62a-4c9de5c763f7)" />
                                </svg>
                            </div>
                            <div className="relative max-w-md px-4 mx-auto sm:max-w-3xl sm:px-6 lg:px-0 lg:max-w-none lg:py-20">
                                {/* Testimonial card*/}
                                <div className="relative pt-64 pb-10 overflow-hidden shadow-xl rounded-xl">
                                    <img
                                        className="absolute inset-0 object-cover w-full h-full"
                                        src="https://images.unsplash.com/photo-1521510895919-46920266ddb3?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&fp-x=0.5&fp-y=0.6&fp-z=3&width=1440&height=1440&sat=-100"
                                        alt=""
                                    />
                                    <div className="absolute inset-0 bg-blue-300" style={{ mixBlendMode: 'multiply' }} />
                                    <div className="absolute inset-0 bg-gradient-to-t from-blue-200 via-blue-600 to-green-200 opacity-60" />
                                    <div className="relative px-8">
                                        <div>
                                            <img
                                                className="h-12"
                                                src="/logos/google.svg"
                                                alt="google"
                                            />
                                        </div>
                                        <blockquote className="mt-8">
                                            <div className="relative text-lg font-medium text-white md:flex-grow">
                                                <svg
                                                    className="absolute top-0 left-0 w-8 h-8 transform -translate-x-3 -translate-y-2 text-rose-400"
                                                    fill="currentColor"
                                                    viewBox="0 0 32 32"
                                                    aria-hidden="true"
                                                >
                                                    <path d="M9.352 4C4.456 7.456 1 13.12 1 19.36c0 5.088 3.072 8.064 6.624 8.064 3.36 0 5.856-2.688 5.856-5.856 0-3.168-2.208-5.472-5.088-5.472-.576 0-1.344.096-1.536.192.48-3.264 3.552-7.104 6.624-9.024L9.352 4zm16.512 0c-4.8 3.456-8.256 9.12-8.256 15.36 0 5.088 3.072 8.064 6.624 8.064 3.264 0 5.856-2.688 5.856-5.856 0-3.168-2.304-5.472-5.184-5.472-.576 0-1.248.096-1.44.192.48-3.264 3.456-7.104 6.528-9.024L25.864 4z" />
                                                </svg>
                                                <p className="relative">
                                                    Tincidunt integer commodo, cursus etiam aliquam neque, et. Consectetur pretium in volutpat,
                                                    diam. Montes, magna cursus nulla feugiat dignissim id lobortis amet.
                                                </p>
                                            </div>

                                            <footer className="mt-4">
                                                <p className="text-base font-semibold text-rose-200">Sarah Williams, CEO at Workcation</p>
                                            </footer>
                                        </blockquote>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="relative max-w-md px-4 mx-auto sm:max-w-3xl sm:px-6 lg:px-0">
                            {/* Content area */}
                            <div className="pt-12 sm:pt-16 lg:pt-24">
                                <h2 className="text-3xl font-extrabold tracking-tight text-gray-600 sm:text-4xl">
                                    Get 200% Salary like Neenu
                                </h2>
                                <div className="mt-6 space-y-6 text-gray-500">
                                    <p className="text-md">
                                        Sagittis scelerisque nulla cursus in enim consectetur quam. Dictum urna sed consectetur neque
                                        tristique pellentesque. Blandit amet, sed aenean erat arcu morbi. Cursus faucibus nunc nisl netus
                                        morbi vel porttitor vitae ut. Amet vitae fames senectus vitae.
                                    </p>
                                </div>
                            </div>

                            {/* Stats section */}
                            <div className="mt-10">
                                <dl className="grid grid-cols-2 gap-x-4 gap-y-8">
                                    {getStats(enableStats)}
                                </dl>
                                <div className="mt-10">
                                    <a href="#" className="text-base font-medium text-blue-600">
                                        Learn more about upcoming cohorts;
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </main>


        </div>
    )

}
