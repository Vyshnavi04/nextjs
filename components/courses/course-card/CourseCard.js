import Link  from 'next/link'
const CourseCard =  ({ course }) => {
    return (
        <div key={course.id} className="flex flex-col max-h/2 rounded-md shadow-sm overflow-hidden">
            <div className="flex-shrink-0">
                <img className="h-48 w-full object-cover bg-blue-600" src={course.imageUrl} alt=""/>
            </div>
            <div className="flex-1 bg-white p-6 flex flex-col justify-between">
                <div className="flex-1">
                    <span className="inline-flex items-center px-3 py-0.5 rounded-full text-sm font-medium bg-green-100 text-blue-800">
                         {course.category.name}
                    </span>
                    <a href={course.href} className="block mt-2">
                        <p className="text-xl font-medium text-blue-900">{course.title}</p>
                        <p className="mt-3 text-base text-gray-500">{course.preview}</p>
                    </a>
                </div>
                <div className="mt-6 flex items-center">
                    <div className="flex-shrink-0">
                        <a href={course.author.href}>
                            <img className="h-10 w-10 rounded-full" src={course.author.imageUrl} alt={course.author.name}/>
                        </a>
                    </div>
                    <div className="ml-3">
                        <Link href={course.author.href}>
                            <div>
                                <p className="text-sm font-medium text-gray-900">
                                    <a href={course.author.href} className="hover:underline">
                                        {course.author.name}
                                    </a>
                                </p>
                                <div className="flex space-x-1 text-sm text-gray-500">
                                    <time dateTime={course.datetime}>{course.date}</time>
                                </div>
                            </div>
                        </Link>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default CourseCard;
