import React, { Component } from "react";
import Toggle from "../../ui/toggle/Toggle";
import CourseCard from "../course-card/CourseCard";
import {Transition} from "@headlessui/react";
import CheckBox from "../../ui/checkbox/CheckBox";
import {FilterIcon} from "@heroicons/react/outline";

const blogPosts = [
    {
        id: 1,
        title: 'Java Full Stack',
        href: '#',
        date: 'Mar 16, 2020',
        datetime: '2020-03-16',
        category: { name: 'Java', href: '#' },
        imageUrl:
            'https://images.unsplash.com/photo-1496128858413-b36217c2ce36?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1679&q=80',
        preview:
            'Learn Java',
        author: {
            name: 'Roel Aufderehar',
            imageUrl:
                'https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80',
            href: '#',
        },
        readingLength: '6 min',
        show: false
    },
    {
        id: 2,
        title: 'How to use search engine optimization to drive sales',
        href: '#',
        date: 'Mar 10, 2020',
        datetime: '2020-03-10',
        category: { name: 'Java', href: '#' },
        imageUrl:
            'https://images.unsplash.com/photo-1547586696-ea22b4d4235d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1679&q=80',
        preview:
            'Lorem ipsum dolor sit amet consectetur adipisicing elit. Velit facilis asperiores porro quaerat doloribus, eveniet dolore. Adipisci tempora aut inventore optio animi., tempore temporibus quo laudantium.',
        author: {
            name: 'Brenna Goyette',
            imageUrl:
                'https://images.unsplash.com/photo-1550525811-e5869dd03032?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80',
            href: '#',
        },
        readingLength: '4 min',
        show: false
    },
    {
        id: 3,
        title: 'Improve your customer experience',
        href: '#',
        date: 'Feb 12, 2020',
        datetime: '2020-02-12',
        category: { name: 'QA', href: '#' },
        imageUrl:
            'https://images.unsplash.com/photo-1492724441997-5dc865305da7?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1679&q=80',
        preview:
            'Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint harum rerum voluptatem quo recusandae magni placeat saepe molestiae, sed excepturi cumque corporis perferendis hic.',
        author: {
            name: 'Daniela Metz',
            imageUrl:
                'https://images.unsplash.com/photo-1487412720507-e7ab37603c6f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80',
            href: '#',
        },
        readingLength: '11 min',
        show: false
    }]

class CourseGrid extends Component {
     category = [
        {
            id: 1,
            name: 'Java'
        },
         {
             id: 2,
             name: 'QA'
         },
         {
             id: 3,
             name: 'AWS'
         },
         {
             id: 4,
             name: 'Cloud'
         },
         {
             id: 5,
             name: 'BA'
         }
    ]


    onSet (val){
         console.log(val)
        if(val.enabled) {
            this.state.filters.push(val.name)
            this.setState({
                filters: this.state.filters
            })
        }else {
            const index = this.state.filters.indexOf(val.name);
            if(index>-1) {
                this.state.filters.splice(index,1)
                this.setState({
                    filters: this.state.filters

                })
            }
        }

    }
    constructor(props) {
      super(props)
        this.state = {
          filterData : blogPosts,
          filters: []
        }
        this.onSet = this.onSet.bind(this);
    }
    render() {
        let  { filterData, filters } = this.state;
        const isShowing = true;
        console.log(filterData)
        console.log(filters)
        console.log('this was filter')
        if(filters.length!==0){
            console.log('Inside when array is not empty')
            filterData = filterData.filter(item => filters.includes(item.category.name)).map(s=>{ s.show=true; return s})
        }else {
            filterData = filterData.map(s=>{ s.show=true; return s});
        }
        return(
            <React.Fragment>
                <section className="bg-gray-50">
                <div className="relative max-w-7xl mx-auto py-4 px-4 sm:px-6 lg:py-8 lg:px-8">
                    <div className="grid grid-cols-4 gap-4">
                        <div className="shadow-sm border-2 border-gray-300 rounded-lg">
                            <div className="flex bg-gray-100 justify-between rounded-t-lg px-5 py-5 border-b-2 border-gray-300">
                                <h2 className="text-lg text-center text-blue-900">Filter By Category</h2>
                                <div>
                                    <svg className="w-6 h-6 text-blue-900"  fill="none" stroke="currentColor" viewBox="0 0 24 24"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"
                                              d="M12 6V4m0 2a2 2 0 100 4m0-4a2 2 0 110 4m-6 8a2 2 0 100-4m0 4a2 2 0 110-4m0 4v2m0-6V4m6 6v10m6-2a2 2 0 100-4m0 4a2 2 0 110-4m0 4v2m0-6V4"/>
                                    </svg>
                                </div>
                            </div>
                            <div className="bg-white px-4">
                                { this.category.map(
                                    cat=>( <CheckBox key={cat.id} name={cat.name} onSet={this.onSet}/>)
                                )}
                            </div>

                        </div>
                        <div className="col-span-3  px-5 rounded-md">
                            <div className="grid grid-cols-2 gap-4">
                                {filterData.map(
                                        blog=> (
                                            <Transition
                                                show={blog.show}
                                                appear={true}
                                                enter="transition-opacity duration-1000"
                                                enterFrom="opacity-0"
                                                enterTo="opacity-100"
                                                leave="transition-opacity duration-250"
                                                leaveFrom="opacity-100"
                                                leaveTo="opacity-0"
                                            >
                                            <CourseCard key={blog.id} course= {blog} />
                                            </Transition>
                                        )
                                )}
                            </div>
                        </div>
                    </div>
                </div>
                </section>
            </React.Fragment>
        )
    }
}
export default CourseGrid;
