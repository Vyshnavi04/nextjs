import { ChevronRightIcon, StarIcon } from "@heroicons/react/solid";
export default function LogoCloud(props) {
  return (
    <div className="bg-gray-50 shadow-sm py-10">
      <div className="max-w-7xl mx-auto  sm:py-12 lg:py-4 md:py-16 px-4 sm:px-6 lg:px-8">
        <div className="mt-2 sm:mt-6">
          <div className="text-center sm:flex-col text-sm font-semibold uppercase text-gray-500 tracking-wide">
            <div className="inline-flex items-center divide-x divide-gray-300">
              <div className="flex-shrink-0 flex pr-5">
                <StarIcon
                  className="h-5 w-5 text-yellow-400"
                  aria-hidden="true"
                />
                <StarIcon
                  className="h-5 w-5 text-yellow-400"
                  aria-hidden="true"
                />
                <StarIcon
                  className="h-5 w-5 text-yellow-400"
                  aria-hidden="true"
                />
                <StarIcon
                  className="h-5 w-5 text-yellow-400"
                  aria-hidden="true"
                />
                <StarIcon
                  className="h-5 w-5 text-yellow-400"
                  aria-hidden="true"
                />
              </div>
              <div className="min-w-0 flex-1 pl-5 py-1 text-sm text-gray-500 sm:py-3">
                <span className="font-medium text-gray-900">Rated 5 stars</span>{" "}
                by over{" "}
                <span className="font-medium text-gray-900">
                  500+ Learners for Major Platforms
                </span>
              </div>
            </div>
          </div>
        </div>
        <div className="mt-6 grid grid-cols-2 gap-8 md:grid-cols-6 lg:grid-cols-5">
          <div className="col-span-1 flex justify-center md:col-span-2 lg:col-span-1">
            <img className="h-12" src="logos/google.svg" alt="Tuple" />
          </div>
          <div className="col-span-1 flex justify-center md:col-span-2 lg:col-span-1">
            <img className="h-12" src="logos/facebook.svg" alt="Mirage" />
          </div>
          <div className="col-span-1 flex justify-center md:col-span-2 lg:col-span-1">
            <img
              className="h-12"
              src="https://d33wubrfki0l68.cloudfront.net/57c62ce0632ed585c93bc5b803d5161656901368/67c4b/images/press/course_report.png"
              alt="StaticKit"
            />
          </div>
          <div className="col-span-1 flex justify-center md:col-span-2 md:col-start-2 lg:col-span-1">
            <img
              className="h-12"
              src="https://tailwindui.com/img/logos/transistor-logo-gray-400.svg"
              alt="Transistor"
            />
          </div>
          <div className="col-span-2 flex justify-center md:col-span-2 md:col-start-4 lg:col-span-1">
            <img
              className="h-12"
              src="https://tailwindui.com/img/logos/workcation-logo-gray-400.svg"
              alt="Workcation"
            />
          </div>
        </div>
      </div>
    </div>
  );
}
