const PageHeader = ({title, img})=> {
    return(
        <div className="relative">
            <div className="absolute inset-x-0 bottom-0 h-1/3 " />
                <div className="mx-auto">
                    <div className="relative shadow-xl w-full sm:overflow-hidden">
                        <div className="absolute inset-0">
                            <img
                                className="h-full w-full object-cover"
                                src={img? img:"https://images.unsplash.com/photo-1521737852567-6949f3f9f2b5?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=2830&q=80&sat=-100"}
                                alt="People working on laptops"
                            />
                            <div
                                className="absolute inset-0 bg-gradient-to-r from-blue-900 to-green-500"
                                style={{ mixBlendMode: 'multiply' }}
                            />
                        </div>
                        <div className="relative h-64 px-4 py-16 sm:px-6 sm:py-24 lg:py-16 lg:px-8">
                            <div className="max-w-7xl mx-auto px-10">
                            <h1 className="text-2xl font-bold tracking-tight sm:text-3xl lg:text-5xl">
                                <span className="block text-white">{title}</span>
                            </h1>
                            </div>
                        </div>
                    </div>
              </div>
        </div>
    )
}
export default PageHeader;
