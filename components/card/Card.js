/* This example requires Tailwind CSS v2.0+ */
import { CheckCircleIcon } from '@heroicons/react/solid'
import UpComing from "../upcoming/UpComing";
const includedFeatures = [
    'Private forum access',
    'Member resources',
    'Entry to annual conference',
    'Official member t-shirt',
]
export default function FeaturedCard({title, description}) {
    return (
        <div className="mt-4 pb-8 sm:mt-4 sm:pb-5 lg:pb-5">
            <div className="relative">
                <div className="absolute inset-0 " />
                <div className="relative max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
                    <div className="max-w-lg mx-auto rounded-lg shadow-lg overflow-hidden lg:max-w-none lg:flex">
                        <div className="flex-1 bg-white px-6 py-8 lg:p-12">
                            <h3 className="text-2xl font-extrabold text-gray-900 sm:text-3xl">Lifetime Membership</h3>
                            <p className="mt-6 text-base text-gray-500">
                                Lorem ipsum dolor sit amet consect etur adipisicing elit. Itaque amet indis perferendis blanditiis
                                repellendus etur quidem assumenda.
                            </p>
                            <div className="mt-8">
                                <div className="flex items-center">
                                    <h4 className="flex-shrink-0 pr-4 bg-white text-sm tracking-wider font-semibold uppercase text-blue-600">
                                        What's included
                                    </h4>
                                    <div className="flex-1 border-t-2 border-gray-200" />
                                </div>
                                <ul className="mt-8 space-y-5 lg:space-y-0 lg:grid lg:grid-cols-2 lg:gap-x-8 lg:gap-y-5">
                                    {includedFeatures.map((feature) => (
                                        <li key={feature} className="flex items-start lg:col-span-1">
                                            <div className="flex-shrink-0">
                                                <CheckCircleIcon className="h-5 w-5 text-green-400" aria-hidden="true" />
                                            </div>
                                            <p className="ml-3 text-sm text-gray-700">{feature}</p>
                                        </li>
                                    ))}
                                </ul>
                            </div>
                        </div>
                        <div className="py-8 px-6 text-center bg-gray-50 lg:flex-shrink-0 lg:flex lg:flex-col lg:justify-center lg:p-12">
                            <p className="text-lg leading-6 font-medium text-gray-900 pb-3">UpComing Cohorts</p>
                            <UpComing></UpComing>
                            <div className="mt-6">
                                <div className="rounded-md shadow">
                                    <a
                                        href="#"
                                        className="flex items-center justify-center px-5 py-3 border border-transparent text-base font-medium rounded-md text-white bg-blue-800 hover:bg-blue-900"
                                    >
                                        Get Access
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
