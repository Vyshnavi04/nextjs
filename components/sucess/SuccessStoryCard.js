export default function SuccessStoryCard() {
    return(
        <div className="sm:mb-3 md:mb-0  overflow-hidden shadow sm:rounded-lg">
            <div className="px-4 py-3">
                <div className="flex">
                    <div className="mr-4 flex-shrink-0 self-center">
                        <img src="/assests/testimonial.jpeg" className="h-32 w-32 blur-lg rounded-full object-cover"/>
                    </div>
                    <div className="py-8">
                        <h4 className="text-lg font-bold">Alisha T</h4>
                        <p className="mt-1">
                            Repudiandae sint consequuntur vel. Amet ut nobis explicabo numquam expedita quia omnis voluptatem. Minus
                            quidem ipsam quia iusto.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    )
}
