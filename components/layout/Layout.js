import Head from "next/head";
import Router from 'next/router';
import NProgress from 'nprogress'

import Navbar from "../header/navbar";
import Footer from "../footer/Footer";

Router.onRouteChangeStart = () => NProgress.start();
Router.onRouteChangeComplete = () => NProgress.done();
Router.onRouteChangeError = () => NProgress.done();




const Layout  = ({children, title})=> (
            <div>
                <Head>
                    <title>{title}</title>
                </Head>
                <Navbar/>
                {children}
                <Footer/>
            </div>
)

export default Layout;
