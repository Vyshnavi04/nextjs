import {
    PlayIcon,
    CheckCircleIcon,
    PhoneIcon,
    CalendarIcon,
    PhoneOutgoingIcon,
    DocumentAddIcon,
    MailIcon
} from '@heroicons/react/outline';
import {Popover} from "@headlessui/react";

function classNames(...classes) {
    return classes.filter(Boolean).join(' ')
}
  
export default function CallToAction({ callsToAction }) {
    return (
        <div className="bg-blue-600">
            <div className="px-4 py-5 mx-auto space-y-6 max-w-7xl sm:flex sm:space-y-0 sm:space-x-10 sm:px-6 lg:px-8">
            {callsToAction.map((item) => (
              <div key={item.name} className="flow-root">
                <a
                  href={item.href}
                  className={classNames( item.highlight ? 'bg-green-600': 'bg-blue-600', ' flex items-center p-3 -m-3 text-base font-medium text-gray-200 rounded-md hover:bg-blue-800')}>
                    <item.icon className="flex-shrink-0 w-6 h-6 text-gray-100" aria-hidden="true" />
                  <span className="ml-3">{item.name}</span>
                </a>
              </div>
            ))}
          </div>
        </div>

    );
}
