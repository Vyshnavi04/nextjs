import {PlayIcon} from "@heroicons/react/outline";

const TestMonialCard = (props) =>{
    return (
        <div className="relative max-w-md clear-both h-full mx-auto py-12 px-4 sm:max-w-7xl sm:px-6 lg:py-16 lg:px-8">
            <div className="max-w-3xl mx-auto text-center">
                <h2 className="text-3xl font-bold text-gray-500 uppercase pb-5">What our learners say</h2>
              </div>
            <div className="flex md:flex-row sm:flex-col justify-center items-center ">
                <div className="md:row-span-2 h-full w-96 sm:mb-3 md:mb-0  overflow-hidden ">
                    <div className="relative mr-4">
                        <img src="/assests/testimonial.jpeg" className="h-96 w-96 blur-lg sm:rounded-lg object-cover"/>
                        <div className="absolute z-30 w-28 h-28 top-1/3 left-36  text-white">
                            <PlayIcon></PlayIcon>
                        </div>
                    </div>
                </div>
                <div className="flex flex-col md:w-1/2 sm:w-full md:px-0 sm:py-32">
                    <div className="h-1/2">
                        <div className="sm:mb-3 md:mb-0 overflow-hidden shadow sm:rounded-lg">
                            <div className="px-4 py-3 ">
                                <div className="flex md:flex-row sm:flex-col ">
                                    <div className="mr-4 flex-shrink-0 self-center">
                                        <img src="/assests/testimonial.jpeg" className="h-32 w-32 blur-lg rounded-full object-cover"/>
                                    </div>
                                    <div className="py-8">
                                        <h4 className="text-lg font-bold">Alisha T</h4>
                                        <p className="mt-1">
                                            Repudiandae sint consequuntur vel. Amet ut nobis explicabo numquam expedita quia omnis voluptatem. Minus
                                            quidem ipsam quia iusto.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="h-1/2">
                        <div className="h-full  sm:mb-3 md:mb-0  overflow-hidden shadow sm:rounded-lg">
                            <div className="px-4 py-3">
                                <div className="flex">
                                    <div className="mr-4 flex-shrink-0 self-center">
                                        <img src="/assests/testimonial.jpeg" className="h-32 w-32 blur-lg rounded-full object-cover"/>
                                    </div>
                                    <div className="py-8">
                                        <h4 className="text-lg font-bold">Alisha T</h4>
                                        <p className="mt-1">
                                            Repudiandae sint consequuntur vel. Amet ut nobis explicabo numquam expedita quia omnis voluptatem. Minus
                                            quidem ipsam quia iusto.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default TestMonialCard;